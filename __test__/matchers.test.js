describe('comparadores',()=>{
  const user ={
    name:"arturo",
    lastname:"guerrero"
  }
  const user2 ={
    name:"aime",
    lastname:"lopez"
  }

  test('igualdad de elementos', ()=>{
    expect(user).toEqual(user)
  })

  test('no igualdad de elementos', ()=>{
    expect(user).not.toEqual(user2)
  })
})