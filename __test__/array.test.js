import {arrayFruits, arrayColors} from '../arrays'

describe('comprobamos los arrays',() => {
  test('¡tiene una fruta en especifico ?',() =>{
    expect(arrayFruits()).toContain('sandia')
  })
  test('no contiene una fruta',()=>{
    expect(arrayFruits()).not.toContain('papaya')
  })
  test('tamaño del areglo',()=>{
    expect(arrayFruits()).toHaveLength(5)
  })
  test('existe el color ?',() =>{
    expect(arrayColors()).toContain('rosa')
  })

})