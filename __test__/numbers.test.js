import {numbers} from '../numbers'

describe('comparacion de numeros', () => {
  
  test('mayor que',()=>{
    expect(numbers(2,2)).toBeGreaterThan(3)
  })

  test('mayor o igual que',()=>{
    expect(numbers(2,2)).toBeGreaterThanOrEqual(4)
  })

  test('mayor o igual que',()=>{
    expect(numbers(2,1)).toBeLessThan(4)
  })

})