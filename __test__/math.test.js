import {sumar,mult,restar,divide} from '../math'

describe('Calculos matematicos',()=>{
  test('prueba sumas',()=>{
    expect(sumar(1,1)).toBe(2)
  })

  test('prueba Multiplicar',()=>{
    expect(mult(4,2)).toBe(8)
  })

  test('prueba Divdir',()=>{
    expect(divide(4,2)).toBe(2)
  })

  test('prueba restar',()=>{
    expect(restar(4,2)).toBe(2)
  })
})