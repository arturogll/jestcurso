import {isNull,isFalse,isTrue,isUndefined} from '../true'

describe('probar resultados nulos',()=>{
  
  test('null',()=>{
    expect(isNull()).toBeNull()
  })

  test('true',()=>{
    expect(isTrue()).toBeTruthy()
  })

  test('false',()=>{
    expect(isFalse()).toBeFalsy()
  })

  test('undefined',()=>{
    expect(isUndefined()).toBeUndefined()
  })

})